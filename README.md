## ***Pirmas bandymas*** :+1:
###### Pirmas mano mazas projekciukas [Python script to import csv into MySQL](https://gitlab.com/slkyper/python_script_to_mysql). :snake:

## Paprasta instrukcija :sunglasses: , kas supras , o kas ne :sweat_smile:
###### **Galite atsisiusti arba tesiog  git clone** https://gitlab.com/slkyper/python_script_to_mysql.git
###### Atsisiunte instaliuokite requirements.txt -> pip install -r requirements.txt

###### Jungtis per cmd\Terminala -> macOS :apple:
###### Atsidaryti Terminala ir susirasti direktorija , kurioje yra visi sie failai. -> ls - rodo failus , cd keicia direktorija.
###### Paziurekite kokia versija -> pip --version.
###### Atnaujinkite pip jei reikia -> python3 -m pip install --upgrade pip
###### Butinai instaliuokite virtuale aplinka, jei neturite ->  pip install virtualenv
###### Turi buti aktyvuota virtauli aplinka -> source venv/bin/activate
###### Galite pasiziureti kokie paketai instaliuoti  -> pip freeze . Jei norite iseiti is venv -> deactivate
###### Toliau galime paleisti cinemaDB.py -> python3 cinemaDB.py

#
###### Jei viskas pavyko , toliau per terminala gausite irasyti host,user,password ir savo duomenu bazes pavadinima.
###### host , user , password rasykite tuos duomenu bazes , kuria esate sukuria , arba sukurkite is naujo.
###### Dazniausiai: host = localhost , user = root , password = jusu sugalvotas..
